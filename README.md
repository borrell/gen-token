# gen-token 1.0.0
Copyright (C) 2019 gen-token

## Usage:
Easy generation of AWS STS tokens when authenticating with a federated SAML identity provider.

Select a role from a list provided by the SAML provider:

  _gen-token --saml-provider {url}_

Assume a known role based on the ARN:

  _gen-token --idp-name {idpProviderName} --role-arn {roleArn} --saml-provider {url}_

## Command Line Options:
|Command              | Description
|---------------------|-----------------
| _--verbose_         | Enable verbose messages.
|  _--debug_          | Enable debug messages.
|  _--saml-provider_  | Optional. Set the SAML Provider URL. This should be in the format http{s}://adfs.corp.com/adfs/ls/idpinitiatedsignon?loginToRp=urn:amazon:webservices. NB: One of either '--saml-provider' or '--use-saml-file' MUST be specified.
| _--use-saml-file_    | Optional. Use the default SAML file for testing. This option will use the SAML response found in the file 'idpinitiatedsignon.txt' in the program directory. NB: One of either '--saml-provider' or '--use-saml-file' MUST be specified.
| _--idp-name_        | Optional. Set the Identity Provider name. This is used to assume a known role. If using this parameter, both '--idp-name' and '--role-arn' MUST be specified.
| _--role-arn_        | Optional. Set the ARN of the role you want to assume. This is used to assume a known role. If this parameter is not specified, the user will be asked to select a role from a list. If using this parameter, both '--idp-name' and '--role-arn' MUST be specified.
| _--duration_        | Optional. The duration, in seconds, of the role session. Must be minimum of 900 seconds if specified. Default value is 3600.
| _--output_          | Optional. Set the type of output. Valid values are: config, json, envvars-nix, envvars-win, stdout. Default output is stdout.
| _--help_            | Display this help screen.
| _--version_         | Display version information.
