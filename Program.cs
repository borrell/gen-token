﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

using CommandLine;
using CommandLine.Text;

using NSoup;

using System.Net.Http;
using System.Collections.Generic;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.SecurityToken.SAML;
using Amazon.Runtime;

using HtmlAgilityPack;

namespace gen_token
{
    class Program
    {

        private static class Opts
        {
            public static bool verbose { get; set; }
            public static bool debug { get; set; }
            public static string SAMLProvider { get; set; }
            public static bool SAMLFile { get; set; }
            public static string IDPname { get; set; }
            public static string RoleArn { get; set; }
            public static int Duration { get; set; }
            public static string Output { get; set; }
        }

        private class Options
        {
            [Usage(ApplicationAlias = "gen-token")]
            public static IEnumerable<Example> Examples {
                get {
                    return new List<Example>() {
                        new Example(
                            "Select a role from a list provided by the SAML provider",
                            new Options { SAMLProvider = "{url}"}
                        ),
                        new Example(
                            "Assume a known role based on the ARN",
                            new Options { SAMLProvider = "{url}", RoleArn = "{roleArn}", IDPname = "{idpProviderName}" }
                        )
                    };
                }
            }

            [Option("verbose", Required = false, HelpText = "Enable verbose messages.")]
            public bool Verbose { get; set; }

            [Option("debug", Required = false, HelpText = "Enable debug messages.")]
            public bool Debug { get; set; }

            [Option("saml-provider", Required = false,
                HelpText =
                    "Optional. Set the SAML Provider URL. This should be in the format " +
                    "http{s}://adfs.corp.com/adfs/ls/idpinitiatedsignon?loginToRp=urn:amazon:webservices. " +
                    "NB: One of either '--saml-provider' or '--use-saml-file' MUST be specified."
            )]
            public string SAMLProvider { get; set; }

            [Option("use-saml-file", Required = false,
                HelpText =
                    "Optional. Use the default SAML file for testing. This option will use the SAML response " +
                    "found in the file 'idpinitiatedsignon.txt' in the program directory. " +
                    "NB: One of either '--saml-provider' or '--use-saml-file' MUST be specified."
            )]
            public bool SAMLFile { get; set; }

            [Option("idp-name", Required = false,
                HelpText =
                    "Optional. Set the Identity Provider name. This is used to assume a known role. If using this " +
                    "parameter, both '--idp-name' and '--role-arn' MUST be specified."
            )]
            public string IDPname { get; set; }

            [Option("role-arn", Required = false,
                HelpText =
                    "Optional. Set the ARN of the role you want to assume. This is used to assume a known role. If this " +
                    "parameter is not specified, the user will be asked to select a role from a list. " +
                    "If using this parameter, both '--idp-name' and '--role-arn' MUST be specified."
            )]
            public string RoleArn { get; set; }

            [Option("duration", Required = false,
                HelpText =
                    "Optional. The duration, in seconds, of the role session. Must be minimum of 900 seconds if specified. " +
                    "Default value is 3600."
            )]
            public int Duration { get; set; }

            [Option("output", Required = false,
                HelpText =
                    "Optional. Set the type of output. Valid values are: config, json, envvars-nix, envvars-win, stdout. " +
                    "Default output is stdout."
            )]
            public string Output { get; set; }
        }

        private static void Main(string[] args)
        {
            //Parse options from the command line into object
            Parser.Default.ParseArguments<Options>(args)
                .WithParsed<Options>(o => {
                    Opts.verbose = o.Verbose;
                    Opts.debug = o.Debug;
                    Opts.IDPname = o.IDPname;
                    Opts.RoleArn = o.RoleArn;
                    Opts.Output = o.Output;

                    Opts.SAMLFile = o.SAMLFile;
                    if (String.IsNullOrEmpty(o.SAMLProvider)) {
                        Opts.SAMLProvider = "";
                    } else {
                        Opts.SAMLProvider = o.SAMLProvider;
                    }

                    if (Opts.Duration == 0) {
                        Opts.Duration = 3600;
                    } else {
                        Opts.Duration = o.Duration;
                    }

                });

            //Work through possible flow options
            //Require at least either SAML Provider or SAML File flag
            if ( ( String.IsNullOrEmpty(Opts.SAMLProvider) ^ Opts.SAMLFile )) {
                Console.WriteLine("Specify either a SAML Provider (--saml-provider), or provide a SAML file (--use-saml-file)");
                Environment.Exit(1);
            }
            //Require either both or neither RoleARN or IDP Name
            if (String.IsNullOrEmpty(Opts.RoleArn) ^ String.IsNullOrEmpty(Opts.IDPname)) {
                Console.WriteLine("Specify both a Role ARN (--role-arn) and an IDP Name (--idp-name), or specify neither");
                Environment.Exit(1);
            }
            //Ensure duration is at least 900 seconds
            if (Opts.Duration < 900) {
                Console.WriteLine("Duration must be at least 900 seconds (15 mins)");
                Environment.Exit(1);
            }

            //Get the SAML response
            string SAMLResponse = !(String.IsNullOrEmpty(Opts.SAMLProvider)) ?
                fetchSAMLResponse(Opts.SAMLProvider) : fetchSAMLResponse();
            //Parse the Roles from the SAML input
            List<(string Principal, string Role)> roles = parseRolesFromSAMLResponse(SAMLResponse);

            /*  If the Role ARN and the IDP Name are provided, validate that they are in the SAML response
                and use those to generate tokens. If they havn't been provided, ask the user to pick a role
                from the SAML provider
             */
            int role_index = new int();
            if ( !(String.IsNullOrEmpty(Opts.RoleArn)) & !(String.IsNullOrEmpty(Opts.IDPname)) ) {
                role_index = validateUserRoleChoice(roles);
            } else {
                //Ask the user which role they want to assume
                role_index = getUserRoleChoice(roles);
            }

            //Fetch credentials
            fetchCredentials(roles[role_index].Role, roles[role_index].Principal, SAMLResponse, Opts.Duration);
        }

        private static int getUserRoleChoice(List<(string Principal, string Role)> roles)
        {
            Console.WriteLine("Available Roles:");
            for(int i = 0; i < roles.Count; i++) {
                Console.WriteLine($"[{i}] {roles[i].Role}");
            }

            Console.Write("Enter the role number you want to assume: ");
            string input = Console.ReadLine();

            int role_index = int.Parse(input);
            if (role_index >= 0 && role_index < roles.Count) {
                //Valid input
                return role_index;
            } else {
                Console.WriteLine("Input is not a valid choice");
                Environment.Exit(1);
                return(1);
            }
        }

        private static int validateUserRoleChoice(List<(string Principal, string Role)> roles) {
            var role_index = new int();
            for(int i = 0; i < roles.Count; i++) {
                if (String.Equals(roles[i].Role, Opts.RoleArn) && roles[i].Principal.EndsWith(Opts.IDPname)) {
                    WriteVerbose("Provided role and IDP matches response from SAML provider.");
                    return role_index;
                }
            }
            //User provided both Role and IDP name. Check that they're in the SAML response
            Console.WriteLine("Provided Role ARNs and IDP name do not match SAML response");
            Environment.Exit(1);
            return(1);
        }

        private static void fetchCredentials(string RoleArn, string PrincipalArn, string SAMLAssertion, int Duration)
        {
            WriteVerbose("Assuming Role");
            WriteDebug($"Role ARN: {RoleArn}");
            WriteDebug($"Principal ARN: {PrincipalArn}");
            WriteDebug($"Duration: {Duration}");
            WriteDebug($"SAML Assertion: \n{SAMLAssertion}");

            AmazonSecurityTokenServiceClient client = new AmazonSecurityTokenServiceClient();
            AssumeRoleWithSAMLRequest req = new AssumeRoleWithSAMLRequest();

            req.RoleArn = RoleArn;
            req.PrincipalArn = PrincipalArn;
            req.DurationSeconds = Duration;
            req.SAMLAssertion = SAMLAssertion;

            Task<AssumeRoleWithSAMLResponse> res = client.AssumeRoleWithSAMLAsync(req);
            try {
                res.Wait();
            }
            catch (Exception e) {
                Console.WriteLine("An error occured fetching credentials");
                WriteDebug(e.ToString());
                Environment.Exit(1);
            }
            outputCredentials(res.Result.Credentials, RoleArn);
        }

        private static string fetchSAMLResponse(string SAMLProvider)
        {
            WriteVerbose("Fetching SAML Response from URL");

            NSoup.Nodes.Document doc = NSoup.NSoupClient.Parse(new Uri(SAMLProvider), 10 * 1000);
            NSoup.Select.Elements p = doc.GetElementsByAttributeValue("name", "SAMLResponse");
            return p.Val();
        }

        private static string fetchSAMLResponse()
        {
            WriteVerbose("Fetching SAML Response from file");

            NSoup.Nodes.Document doc = NSoup.NSoupClient.Parse(File.Open("idpinitiatedsignon.txt", FileMode.Open), "utf-8");
            NSoup.Select.Elements p = doc.GetElementsByAttributeValue("name", "SAMLResponse");
            return p.Val();
        }

        private static List<(string Principal, string Role)> parseRolesFromSAMLResponse(string SAMLResponse)
        {
            // Return a list of tuples in the format string[principle, role]
            List<(string Principal, string Role)> roles = new List<(string Principal, string Role)>{};

            var doc = new HtmlDocument();
            doc.LoadHtml(Base64Decode(SAMLResponse));

            foreach (var nNode in doc.DocumentNode.Descendants()) {
                if (nNode.FirstChild == null) {
                    if (nNode.InnerText.Contains("arn:aws:iam:")) {
                            string role, principal;
                            if (nNode.InnerText.Split(",")[0].Contains("saml-provider")) {
                                principal = nNode.InnerText.Split(",")[0];
                                role = nNode.InnerText.Split(",")[1];
                            } else {
                                principal = nNode.InnerText.Split(",")[1];
                                role = nNode.InnerText.Split(",")[0];
                            }
                            WriteDebug($"Role ARN: {role}, Principal Arn: {principal}");
                            roles.Add((Principal: principal, Role: role));
                    }
                }
            }
            return roles;
        }

        private static void outputCredentials(Credentials credentials, string RoleArn) {
            switch (Opts.Output) {
                case "config":
                    Regex rgx = new Regex(@"\d{12}");
                    string roleName = RoleArn.Substring(RoleArn.LastIndexOf("/")+1);
                    string acctNum = rgx.Match(RoleArn).Value;
                    Process.Start("aws", $"configure set aws_access_key_id {credentials.AccessKeyId} --profile {acctNum}-{roleName}");
                    Process.Start("aws", $"configure set aws_secret_access_key {credentials.SecretAccessKey} --profile {acctNum}-{roleName}");
                    Process.Start("aws", $"configure set aws_session_token {credentials.SessionToken} --profile {acctNum}-{roleName}");
                    break;
                case "json":
                    Console.WriteLine("\n{");
                    Console.WriteLine($"    \"aws_access_key_id\" : \"{credentials.AccessKeyId}\"");
                    Console.WriteLine($"    \"aws_secret_access_key\" : \"{credentials.SecretAccessKey}\"");
                    Console.WriteLine($"    \"aws_session_token\" : \"{credentials.SessionToken}\"");
                    Console.WriteLine("}\n");
                    break;
                case "envvars-nix":
                    Console.WriteLine($"export AWS_ACCESS_KEY_ID={credentials.AccessKeyId}");
                    Console.WriteLine($"export AWS_SECRET_ACCESS_KEY={credentials.SecretAccessKey}");
                    Console.WriteLine($"export AWS_SESSION_TOKEN={credentials.SessionToken}");
                    break;
                case "envvars-win":
                    Console.WriteLine($"setx AWS_ACCESS_KEY_ID={credentials.AccessKeyId}");
                    Console.WriteLine($"setx AWS_SECRET_ACCESS_KEY={credentials.SecretAccessKey}");
                    Console.WriteLine($"setx AWS_SESSION_TOKEN={credentials.SessionToken}");
                    break;
                case "stdout":
                    Console.WriteLine($"AWS ACCESS KEY ID:      {credentials.AccessKeyId}");
                    Console.WriteLine($"AWS SECRET ACCESS KEY:  {credentials.SecretAccessKey}");
                    Console.WriteLine($"AWS SESSION TOKEN:      {credentials.SessionToken}");
                    break;
                default:
                    goto case "stdout";
            }
            Environment.Exit(0);
        }

        private static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private static void WriteDebug(string message)
        {
            if (Opts.debug) {
                Console.WriteLine(message);
            }
        }

        private static void WriteVerbose(string message)
        {
            if ((Opts.verbose) || (Opts.debug)) {
                Console.WriteLine(message);
            }
        }
    }
}
